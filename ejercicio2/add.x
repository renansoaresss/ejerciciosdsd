typedef string prog<255>;
typedef string pro<255>;

typedef struct namenode *namelist;			/* link in the listing */
 
/* A node in the directory listing */
struct namenode {
	prog clave;						/* name of directory entry */
	prog value;
	namelist next;						/* next entry */
};

struct oasociacion {
	prog clave;
	prog value;
};

typedef oasociacion iarray<>;

struct asociacion {
        int id;
	oasociacion ass;
};

struct basociacion {
	int id;
	pro clave;
};

struct nodo {
	int data;
	struct nodo* sgte;
	iarray lasociacion;
};

union Result switch(int tipo) {
	case 0:
		int tipo;
	case 1:
		string resultado<>;
	default:
		void;
};

union readdir_res switch (int errno) {
	case 0:
		namelist list;		/* no error: return directory listing */
	default:
		void;			/* error occurred: nothing else to return */
};

program ADD_PROG {
        version ADD_VERS {
                Result PonerAsociacion(asociacion) = 1;
                Result ObtenerAsociacion(basociacion) = 2;
                Result BorrarAsociacion(basociacion) = 3;
                readdir_res
		Enumerar(int) = 4;
        } = 1;
} = 0x30090949;
