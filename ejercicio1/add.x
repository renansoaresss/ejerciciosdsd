struct param {
        int a;
        int b;
};

program ADD_PROG {
        version ADD_VERS {
                int SOMA(param) = 1;
		int SUB(param) = 2;
		int DIV(param) = 3;
		int MULT(param) = 4;
        } = 1;
} = 0x23451111;
