/*
 * This is sample code generated by rpcgen.
 * These are only templates and you can use them
 * as a guideline for developing your own functions.
 */

#include "add.h"

int *
soma_1_svc(param *argp, struct svc_req *rqstp)
{

	static int  result;

	/*
	 * insert server code here
	 */
    
    result = argp->a + argp->b;

	return(&result);
}

int *
sub_1_svc(param *argp, struct svc_req *rqstp)
{

	static int  result;

	/*
	 * insert server code here
	 */
    
    result = argp->a - argp->b;

	return(&result);
}

int *
div_1_svc(param *argp, struct svc_req *rqstp)
{

	static int  result;

	/*
	 * insert server code here
	 */
    result = argp->a / argp->b;

	return(&result);
}

int *
mult_1_svc(param *argp, struct svc_req *rqstp)
{

	static int  result;

	/*
	 * insert server code here
	 */
    
    result = argp->a * argp->b;

	return(&result);
}
